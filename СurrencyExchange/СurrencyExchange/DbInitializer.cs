﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entities;

namespace СurrencyExchange
{
    public class DbInitializer: System.Data.Entity.DropCreateDatabaseIfModelChanges<DataContext>
    {
        //Fill fields of db for the first time or when the db model has been changed db is re-created
        protected override void Seed(DataContext context)
        {
            context.CurrencyName.Add(new Entities.DataModels.CurrencyName { Name = "EUR" });
            context.CurrencyName.Add(new Entities.DataModels.CurrencyName { Name = "USD" });
            context.CurrencyName.Add(new Entities.DataModels.CurrencyName { Name = "UAH" });

            context.CurrencyValue.Add(new Entities.DataModels.CurrencyValue { CurrencyNameID = 1, Currency = 1.16M });
            context.CurrencyValue.Add(new Entities.DataModels.CurrencyValue { CurrencyNameID = 2, Currency = 1, IsBaseConvertCurrency = true });
            context.CurrencyValue.Add(new Entities.DataModels.CurrencyValue { CurrencyNameID = 3, Currency = 0.03827M });
            context.SaveChanges();
        }
    }
}