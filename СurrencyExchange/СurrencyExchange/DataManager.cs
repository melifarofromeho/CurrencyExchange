﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Entities;
using Entities.DataModels;

namespace СurrencyExchange
{
    public class DataManager
    {
        private static DataManager repository;

        /// <summary>
        /// Create and return link to AppRepository
        /// </summary>
        public static DataManager Repository
        {
            get
            {
                if (repository == null)
                {
                    repository = new DataManager();
                }
                return repository;
            }
        }

        private DataContext dbContext;
        public DataContext DbContext
        {
            get
            {
                if (dbContext==null)
                {
                    dbContext = new DataContext();
                }
                return dbContext;
            }
        }

        /// <summary>
        /// Return CurrencyValue DB context
        /// </summary>
        public List<CurrencyValue> CurrencyValues
        {
            get
            {
                return DbContext.CurrencyValue.ToList();
            }
        }
    }
}