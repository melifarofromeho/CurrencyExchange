﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Entities.DataModels;

namespace СurrencyExchange.ViewModels
{
    public class CurrencyValueViewModel : CurrencyValue
    {
        public string CurrencyName { set; get; }

        public CurrencyValueViewModel(CurrencyValue entity)
        {
            ID = entity.ID;
            CurrencyNameID = entity.CurrencyNameID;
            Currency = entity.Currency;
            IsBaseConvertCurrency = entity.IsBaseConvertCurrency;
            CurrencyName = DataManager.Repository.DbContext.CurrencyName.FirstOrDefault(a => a.ID == CurrencyNameID).Name;
        }
    }
}