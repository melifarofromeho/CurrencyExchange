﻿{
	function currencyResult(elem) {

		var fromField = $("#from :selected");
		var toField = $("#to :selected");
		var value = $("#value").val();
		var baseCurrencyName = $(elem).data('assigned-id');

		var result;

		if (value < 0) {
			setResul('', 'Wrong number');
			return;
		}

		var temp = fixOperand(value) * fixOperand(fromField.val());
		if (fromField.text() === toField.text()) {
			result = value;
		}
		else if (toField.text() === baseCurrencyName) {
			result = temp;
		}
		else {
			result = temp / fixOperand(toField.val());
		}

		console.log(result);
		setResul((Math.round(result * 100) / 100), toField.text());
	}

	function fixOperand(operand) {
		return parseFloat(operand.replace(",", "."));
	}

	function setResul(result, currencyName) {
		$('output[id="resultValue"]').val(result + " " + currencyName);
	}
}
