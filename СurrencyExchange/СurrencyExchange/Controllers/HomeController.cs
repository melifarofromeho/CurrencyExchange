﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using СurrencyExchange.ViewModels;

namespace СurrencyExchange.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            var repository = DataManager.Repository;
            var values = repository.CurrencyValues.Select(a => new CurrencyValueViewModel(a)).ToList();
            return View(values);
        }
    }
}