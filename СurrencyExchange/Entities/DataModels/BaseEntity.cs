﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.DataModels
{
    /// <summary>
    /// Base Id for models
    /// </summary>
    public class BaseEntity
    {
        [Key]
        public int ID { set; get; }
    }
}
