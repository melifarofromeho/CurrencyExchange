﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.DataModels
{
    /// <summary>
    ///  Data model for currency value
    /// </summary>
    public class CurrencyValue : BaseEntity
    {
        [Required]
        public int CurrencyNameID { set; get; }
        [Required]
        public decimal Currency { set; get; }

        public bool IsBaseConvertCurrency { set; get; }
    }
}