﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Entities.DataModels
{
    /// <summary>
    ///  Data model for currency name
    /// </summary>
    public class CurrencyName : BaseEntity
    {
        [Required]
        public string Name { set; get; }
    }
}
