﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Entities.DataModels;

namespace Entities
{
    /// <summary>
    /// Create DB using models
    /// </summary>
    public class DataContext : DbContext
    {
        public DbSet<CurrencyName> CurrencyName { get; set; }
        public DbSet<CurrencyValue> CurrencyValue { get; set; }
        public DataContext() : base("CurrencyDb")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Save decimal precision
            modelBuilder.Entity<CurrencyValue>().Property(x => x.Currency).HasPrecision(11, 6);
            modelBuilder.Entity<CurrencyName>().ToTable("CurrencyName");
            modelBuilder.Entity<CurrencyValue>().ToTable("CurrencyValue");
        }
    }
}
